FROM alpine

RUN set -x \
    && apk add --no-cache php git sudo htop

RUN mkdir -p /app

WORKDIR /app

ADD ./src/ /app/
RUN chmod -R 755 .

CMD [ "php", "-S", "0.0.0.0:8080" ]