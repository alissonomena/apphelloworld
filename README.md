

## Comandos

build

```bash
docker build -t alisson/php .
```

run

```bash
docker run -p 8080:8080 --rm -it alisson/php
```